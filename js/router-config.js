angular.module('app', ['ui.router']).config(function ($stateProvider, $urlRouterProvider) {

  	$urlRouterProvider.otherwise('/all');

  	$stateProvider
    .state('app', {
      url: '/',
      template: '<categories-list></categories-list>',
    })
    .state('app.all', {
      url: '/all',
      template: '<cards-list></cards-list>',
    })
    .state('app.work', {
      url: '/work',
      template: '<cards-list></cards-list>',
    })
    .state('app.personal', {
      url: '/personal',
      template: '<cards-list></cards-list>',
    });
});
