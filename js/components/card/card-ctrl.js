(function () {
  angular.module('app').controller('cardCtrl', ['CardsService', CardController]);
})();

function CardController(cardsService) {
  this.cardsService = cardsService;
}

CardController.prototype.$onInit = function () {
  this.title = '';
  this.curStatus = '';
  this.todos = this.card.todos || [];
};

CardController.prototype.removeCard = function () {
  if (confirm('Do you really want to remove the card?')) {
    this.cardListCtrl.removeCard(this.card.id);
  }
};

CardController.prototype.addTodo = function () {
  if (this.title === '' || this.title.match(/\s/g)) return;
  if (this.cardsService.hasTodo(this.card, this.title)) {
    alert('This todo alredy exists!');
    return this.title = '';
  }
	const todo = {
		title: this.title,
		isComplete: false,
  };
	this.card.todos.push(todo);
	this.title = '';
};

CardController.prototype.removeTodo = function (title) {
	var index = this.card.todos.findIndex(function (item) {
		return item.title === title;
  }, this);
	this.card.todos.splice(index, 1);
};

CardController.prototype.setStatus = function (value) {
  this.curStatus = value;
};
