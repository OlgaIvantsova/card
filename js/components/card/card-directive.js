(function () {
  angular.module('app').directive('card', function () {
  	return {
  		restrict: 'E',
  		templateUrl: 'js/components/card/card-template.html',
  		controller: 'cardCtrl',
  		controllerAs: 'cardCtrl',
  		bindToController: true,
  		scope: {
  			card: '=',
      },
  		require: {
  			cardListCtrl: '^cardsList',
      },
    };
  });
})();
