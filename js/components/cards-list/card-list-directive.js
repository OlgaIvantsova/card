(function () {
	angular.module('app').directive('cardsList', function () {
			return {
				restrict: 'E',
				templateUrl: 'js/components/cards-list/cards-list-template.html',
				controller: 'cardListCtrl',
				controllerAs: 'cardListCtrl',
				bindToController: true,
				require: {
					categoriesCtrl: '^categoriesList',
	    },
	  };
	});
})();
