(function () {
	angular.module('app').controller('cardListCtrl', ['CardsService', CardListCtrl]);
})();

function CardListCtrl(CardsService) {
	this.cardsService = CardsService;
}

CardListCtrl.prototype.$onInit = function () {
	this.title = '';
	this.cards = [];
	this.cardCategory = '';
	this.cardsService.list(this.getCurCategory()).then(function (value) {
		return this.cards = value;
	}.bind(this));
};

CardListCtrl.prototype.addCard = function () {
	var category = this.categoriesCtrl.categories.find(function (item) {
		return item.title === this.cardCategory;
	}, this);
	var obj = {
		title: this.title,
		category: category.id,
	};
	this.cardsService.add(obj).then(function (value) {
		return value;
	});
	this.title = '';
	this.cardCategory = '';
};

CardListCtrl.prototype.removeCard = function (id) {
	this.cardsService.remove(id).then(function () {

	});
};

CardListCtrl.prototype.getCurCategory = function () {
	return this.categoriesCtrl.сurCategory;
};

CardListCtrl.prototype.categoriesList = function () {
	return this.categoriesCtrl.categories.map(function (item) {
		return item.title;
	});
};
