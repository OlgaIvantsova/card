(function () {
	angular.module('app').service('CardsService', ['$q', CardsService]);
})();

function CardsService($q) {

	this.$q = $q;
	this.cards = [
		{
			id: 1,
			title: 'Card 1',
			category: 1,
			todos: [
				{ title: 'Todo 11', isComplete: false },
				{ title: 'Todo 12', isComplete: false },
				{ title: 'Todo 13', isComplete: true },
				{ title: 'Todo 14', isComplete: false },
			],
		},
		{
			id: 2,
			title: 'Card 2',
			category: 2,
			todos: [
				{ title: 'Todo 21', isComplete: false },
				{ title: 'Todo 22', isComplete: false },
				{ title: 'Todo 23', isComplete: true },
				{ title: 'Todo 24', isComplete: false },
			],
		},
	];
}

CardsService.prototype.list = function (category) {
	if (category === '') {
		return this.$q.when(this.cards);
	}
	const cards = this.cards.filter(function (item) {
		return item.category === category;
	});
	return this.$q.when(cards);
};

CardsService.prototype.detail = function (id) {
	var card = this.cards.find(function (item) {
		return item.id === id;
	});
	return this.$q.when(card);
};

CardsService.prototype.remove = function (id) {
	var index = this.cards.findIndex(function (card) {
		return card.id === id;
	});
	this.cards.splice(index, 1);
	return this.$q.when();
};

CardsService.prototype.add = function (obj) {
	const card = {
		id: this.cards.length + 1,
		todos: [],
	};
	Object.assign(card, obj);
	this.cards.push(card);
	return this.$q.when(card);
};

CardsService.prototype.update = function (data) {
	var index = this.cards.findIndex(function (card) {
		return card.id === data.id;
	});
	this.cards[index] = data;
	return this.$q.when(data);
};

CardsService.prototype.save = function (data) {
	if (data.id) {
		return this.update(data);
	}
	return this.add(data);
};

CardsService.prototype.hasTodo = function (card, todo) {
	this.detail(card.id).then(function (value) {
		return card = value;
	});
	return card.todos.some(function (item) {
		return item.title === todo;
	}, this);
};
