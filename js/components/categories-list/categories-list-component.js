(function () {
	angular.module('app').component('categoriesList',
		{
			templateUrl: '/js/components/categories-list/categories-list-template.html',
			controller: 'categoriesCtrl',
		}
	);
})();
