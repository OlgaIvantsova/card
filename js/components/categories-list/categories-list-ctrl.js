(function () {
  angular.module('app').controller('categoriesCtrl', ['CategoriesService', CategoriesController]);
})();

function CategoriesController(CategoriesService) {
  this.categoriesService = CategoriesService;
}

CategoriesController.prototype.$onInit = function (CategoriesService) {
	this.categories = [];
	this.сurCategory = '';
	this.categoriesService.list().then(function (value) {
		return this.categories = value;
  }.bind(this));
};

CategoriesController.prototype.setCategory = function (id) {
  return this.сurCategory = id;
};
