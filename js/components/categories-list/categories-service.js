(function () {
	angular.module('app').service('CategoriesService', ['$q', CategoriesService]);
})();

function CategoriesService($q) {

    this.$q = $q;

		this.categories = [
			{
				id: 1,
				title: 'personal',
			},
			{
				id: 2,
				title: 'work',
			},
		];
	}

		CategoriesService.prototype.list = function () {
			return this.$q.when(this.categories);
		};

		CategoriesService.prototype.detail = function (propValue) {
			var category = this.categories.find(function (item) {
				return item.id === id;
			});
			return this.$q.when(category);
		};

		CategoriesService.prototype.remove = function (id) {
			var index = this.categories.findIndex(function (card) {
				return category.id === id;
			});
			this.categories.splice(index, 1);
			return this.$q.when();
		};

		CategoriesService.prototype.add = function (title) {
			const category = {
				id: this.categories.length + 1,
				title: title,
			};
			this.categories.push(category);
			return this.$q.when(category);
		};
