(function () {
  angular.module('app').directive('todo', function () {
		return {
			restrict: 'E',
			templateUrl: '/js/components/todo/todo-template.html',
			require: {
				cardCtrl: '^card',
      },
			scope: {
				todo: '=',
      },
			controller: 'todoCtrl',
			controllerAs: 'todoCtrl',
			bindToController: true,
    };
  });
})();
