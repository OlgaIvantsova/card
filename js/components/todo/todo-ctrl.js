(function () {
  angular.module('app').controller('todoCtrl', ['CardsService', TodoController]);
})();

function TodoController(cardsService) {
  this.cardsService = cardsService;
}

TodoController.prototype.editTodo = function () {
  const parentId = this.cardCtrl.card.id;
  this.cardsService.detail(parentId).then(function (card) {
  	todo = card.todos.find(function (todo) {
  		return todo.title === this.todo.title;
    }, this);
  	todo.isComplete = !todo.isComplete;
  	this.cardsService.save(card);
  }.bind(this));
};

TodoController.prototype.removeTodo = function () {
	if (confirm('Do you really want to remove the task?')) {
		this.cardCtrl.removeTodo(this.todo.title);
  }
};
